#ifndef LIST_H
#define LIST_H

/*This defines the ADT list for storing data in a BST*/

	#include <stdio.h>
	#include <stdlib.h>
	#include "stack/stack.h"
	#include "private.h"

	typedef struct list *list_t; // ADT for a data list
	list_t list_new(compf cf); // List constructor
	int list_add(list_t lst,void *stdata); // stores your data into specified list
	// return values: 0=ok, -1= Unvalid lst pointer; +1=faild to malloc new node for list

	void list_errorprint(const char* listname, int rvalue); // prints the error text to stderr with the return of list_add()
	unsigned int list_lengh(list_t lst); // tells the client how many objects are in the list
	void* list_search(list_t lst, void *refdata); // searches in the list for data
	void* list_delete(list_t lst, void *rmdata); // deletes the Data of the ADT
	void list_free(list_t lst); // list destructor

	typedef struct Literator *Lit; // ADT Iterator interface
	Lit list_iterator(list_t lst); // Constructor for iface
	int Lit_hasNext(Lit it); // Checks if data is available
	void* Lit_next(Lit it); // Returns the pointed data of the current node
	void Lit_free(Lit it); // Destructor of Iterator ADT
	
#endif
