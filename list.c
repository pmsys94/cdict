#include "list.h"

struct lobject{
	void *data;
	tnode l,r;
};
struct list{
	tnode root;
	compf cpf;
	unsigned int elements;
};
struct Literator{
	list_t lst;
	stack_t stk;
};

void tnode_rfree(tnode lnd){
	if (lnd == NULL) return;
	tnode_rfree(lnd->l);
	tnode_rfree(lnd->r);
	free(lnd);
}
tnode tnode_new(void *data, int *suc){
	tnode newnode = (tnode)malloc(sizeof(*newnode));
	if (newnode == NULL){
		*suc = 1;
		return NULL;
	}
	newnode->data = data;
	newnode->l = newnode->r = NULL;
	*suc = 0;
	return newnode;
}
tnode insert(tnode nd, void *ndData, compf cpf, int *suc){
	if(nd == NULL) return tnode_new(ndData, suc);
	if(cpf(ndData, nd->data) < 0){
		nd->l = insert(nd->l, ndData, cpf, suc);
	} else {
		nd->r = insert(nd->r, ndData, cpf, suc);
	}
	return nd;
}
void* search(tnode nd, void *sdata, compf cpf){
	if(nd == NULL) return NULL; // not found
	int sres = cpf(sdata, nd->data);
	if(sres == 0) { // found item
		return nd->data;
	} else if (sres < 0) { // search sdata in left subtree
		return search(nd->l, sdata, cpf);
	} else { // search sdata in right subtree
		return search(nd->r, sdata, cpf);
	}
}
void* deleteNd(tnode *nd, void *rmkey, compf cpf){
	tnode rmnode;
	void *data;
	if (*nd == NULL) return NULL;// not found
	int compare = cpf(rmkey, (*nd)->data);
	if (compare == 0){ // found
		data = (*nd)->data;
		if ((*nd)->l != NULL && (*nd)->r != NULL){
			rmnode = delmin(&(*nd)->r);
			(*nd)->data = rmnode->data; // copy payload
		} else if ((*nd)->l != NULL){
			rmnode = *nd;
			*nd = (*nd)->l;
		} else {
			rmnode = *nd;
			*nd = (*nd)->r;
		}
		free(rmnode);
		return data;
	} else if (compare < 0){ // left sub delete
		return deleteNd(&(*nd)->l, rmkey, cpf);
	} else { // right sub delete
		return deleteNd(&(*nd)->r, rmkey, cpf);
	}
}
void pushleft(stack_t st, tnode nd){
	while(nd != NULL){
		stack_errorprint(stack_push(st, nd));
		nd = nd->l;
	}
}

tnode delmin(tnode *nd){
	tnode delnd;
	if(nd == NULL) return NULL;
	if((*nd)->l != NULL){
		return delmin(&(*nd)->l);
	} else {
		delnd = *nd;
		*nd = (*nd)->r;
		return delnd;
	}
}

list_t list_new(compf cf){
	list_t newlist = (list_t)malloc(sizeof(*newlist));
	if (newlist == NULL) return NULL;
	newlist->cpf = cf;
	newlist->root = NULL;
	newlist->elements = 0;
	return newlist;
}
int list_add(list_t lst,void *stdata){
	int suc = -1;	
	if (lst == NULL){
		return suc;
	}
	lst->root = insert(lst->root, stdata, lst->cpf, &suc);
	if(suc == 0) (lst->elements)++;
	return suc;
}
void list_errorprint(const char* listname, int rvalue){
	if(rvalue == -1){
		fprintf(stderr, "*** ERROR: Unvalid list pointer given with argument lst for list %s!\n", listname);
	} else if (rvalue == 1){
		fprintf(stderr, "*** ERROR: Memory allocation failed for list %s!\n", listname);
	}
}
unsigned int list_lengh(list_t lst){
	if (lst == NULL) return 0;
	return lst->elements;
}
void* list_search(list_t lst, void *refdata){
	if (lst == NULL) return NULL;
	return search(lst->root, refdata, lst->cpf);
}
void* list_delete(list_t lst, void *rmdata){
	if(lst == NULL) return NULL;
	void* ptr = deleteNd(&(lst->root), rmdata, lst->cpf);	
	if (ptr != NULL) (lst->elements)--;
	return ptr;
}
void list_free(list_t lst){
	if (lst == NULL) return;
	tnode_rfree(lst->root);
	free(lst);
}
Lit list_iterator(list_t lst){
	Lit it = (Lit)malloc(sizeof(*it));
	if (it == NULL) return NULL;
	it->lst = lst;
	it->stk = stack_new();
	if(it->stk == NULL){
		free(it);
		return NULL;
	}
	pushleft(it->stk, lst->root);
	return it;
}
int Lit_hasNext(Lit it){
	if (it == NULL) return 0;
	return stack_lengh(it->stk) > 0;
}
void* Lit_next(Lit it){
	if(it == NULL) return NULL;
	if(!Lit_hasNext(it)) return NULL;
	tnode node = (tnode)stack_pop(it->stk);
	pushleft(it->stk, node->r);
	return node->data;
}
void Lit_free(Lit it){
	if (it != NULL){
		stack_free(it->stk);
		free(it);
	}
}
