#ifndef LIST_PRIV_H
#define LIST_PRIV_H
	typedef struct lobject *tnode; // ADT Node
	typedef int (*compf)(const void *data1, const void *data2);
	void tnode_rfree(tnode lnd); // Recursive node destructor
	tnode tnode_new(void *data, int *suc); // Node constructor
	tnode insert(tnode nd, void *ndData, compf cpf, int *suc);
	void* search(tnode nd, void *sdata, compf cpf);
	void* deleteNd(tnode *nd, void *rmkey, compf cpf);

	void pushleft(stack_t st, tnode nd);
	tnode delmin(tnode *nd);
#endif
