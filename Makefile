all: list.o stack.o

list.o: list.h private.h

stack.o: stack_obj

stack_obj:
	cd stack; ${MAKE} all "CC=${CC}" "CFLAGS=${CFLAGS}"
clean:
	/bin/rm -f *.o; cd stack; ${MAKE} clean;
