#ifndef STACK_H
#define STACK_H

/*This defines the special ADT list for a stack type*/

	#include <stdio.h>
	#include <stdlib.h>

	typedef struct stack *stack_t; // The Stack ADT
	stack_t stack_new(); // Constructor of new Stack
	int stack_push(stack_t st, void *data); // Place data on Stack
	void* stack_pop(stack_t st); // Get data of Stack
	unsigned int stack_lengh(stack_t st); // Retuerns the lengh of stack
	void stack_free(stack_t st); // Destructor of stack ADT
	void stack_errorprint(int rvalue); // Error Printing on stderr for push
	
#endif
