#include "stack.h"

typedef struct stackelement *stnd;
struct stackelement{
	void *data;
	stnd next;
};
struct stack{
	stnd head;
	unsigned int elements;
};
void stnd_rfree(stnd node){
	if (node == NULL) return;
	if (node->next != NULL){
		stnd_rfree(node->next);
	}
	free(node);
}
stack_t stack_new(){
	stack_t st = (stack_t)malloc(sizeof(*st));
	if(st == NULL) return NULL;
	st->head = NULL;
	st->elements = 0;
	return st;
}
int stack_push(stack_t st, void *data){
	if(st == NULL || data == NULL) return -1;
	stnd newnd = (stnd)malloc(sizeof(*newnd));
	if (newnd == NULL) return -2;
	newnd->data = data;
	if(st->head == NULL){
		newnd->next = NULL;
	} else {
		newnd->next = st->head;
	}
	st->head = newnd;
	(st->elements)++;
	return 0;
}
void* stack_pop(stack_t st){
	if(st == NULL) return NULL;
	if(st->elements == 0) return NULL;
	void *data = st->head->data;
	stnd popnd = st->head;
	st->head = st->head->next;
	free(popnd);
	(st->elements)--;
	return data;
}
unsigned int stack_lengh(stack_t st){
	if(st == NULL) return 0;
	return st->elements;
}
void stack_free(stack_t st){
	if(st == NULL) return;
	stnd_rfree(st->head);
	free(st);
}
void stack_errorprint(int rvalue){
	if (rvalue == 0) return;
	switch (rvalue){
		case -1: {
			fprintf(stderr, "*** ERROR: Unvalid pointer given with arguments!\n");
			break;
		}
		case -2: {
			fprintf(stderr, "*** ERROR: Memory allocation failed for stack!\n");
			break;
		}
	}
}
